const app = require("./app");
const port = process.env.PORT || 3000;

const main = async () => {
  app.listen(port, () => {
    console.log(`server is runnig in port ${port}`);
  });
};

main();
